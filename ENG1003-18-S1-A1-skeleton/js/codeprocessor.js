/**
 * Your header documentation here for _listen
 *   For your reference...
 * _listen function will convert the RGBA values received to mean greyscale value .The mean greyscale value and its corresponding timestamp will be used to produce tapcodes. 
 * event will hold an Event object with the pixels in
 * event.detail.data and the timestamp in event.timeStamp
 */

//Defining relationship between tapcode and letters 
let letters  = [
["","","","","",""],    
["","e","t","a","n","d"],
["","o","i","r","u","c"],
["","s","h","m","f","p"],
["","l","y","g","v","j"],
["","w","b","x","q","z"]
];

//Global Variables 
let red = [],green = [],blue = [];
let timeStamp =[];
let colour = [];
let timeRef = timeStamp[0];
let i = 0;
let order = [];
let duration =[];
let timeTaken =[];
let tapCode="";
let N = duration[1];

//Creating the function sum to add up all of the elements in an array 
function sum(a)
{
    let add = 0;
    for (let m=0;m< a.length;m++)
        {
            add +=a[m];
        }
    return add;
}

_listen = function(event)
{
    // Converting the received RGB arrays into a mean greyscale values for the sets of 20x20 pixels
    let array = event.detail.data;
    let greyscale = [];
    for (let n = 0;n < 400;n++)
        {
            red[n] = array[4*n];
            green[n] = array[(4*n)+1];
            blue[n] = array[(4*n)+2];
            greyscale[n] = (red[n] +green[n] +blue[n])/3;
        }
    let meanGreyscale = sum(greyscale)/(greyscale.length);
    
    // Determine the frame of pixels is white or black by considering the mean greyscale value 
    // Store the color of the frame of pixels and its corresponding timestamp into two array 
    if( meanGreyscale >= 100)
        {
            colour[i] = "w";
            timeStamp[i] = event.timeStamp;
            i++;
        }
    else
        {
            colour[i] = "b";
            timeStamp[i] = event.timeStamp;
            i++;
        }
    
    // Determine and store the duration of each tap or gap 
    let j=0;
    for ( let k=0;k<colour.length;k++)
        {
            if ( colour[k] !== colour[k+1])
                {
                    order[j] = colour[k];
                    timeTaken[j] = timeStamp[k];
                    duration[j] = timeStamp[k] - timeRef;
                    timeRef = timeStamp[k+1];
                    j++;
                }
        }

    // Ignore the long black duration at the start of the message 
    //the first duration is the time taken for a tap to run 
    N = duration[1];
    
    // Determine tap , half gap and full gap and convert them to tapcode
    let code = "";
    for (m=1;m< duration.length;m++)
        {
            // the duration will not be exactly 1 N for tap and gap as it will need some time to buffer 
            if(duration[m] <= 1.5*N)
                {
                    if (order[m] === "w")
                        {
                            code += "*";
                             
                        }
                    else
                        {
                            code += "";
                        }
                }
            // If duration is more than 1.5 N , then it is full gap
            else
                {
                    if(order[m] === "b")
                        {
                            code += " ";
                        }
                }
        }
        
    tapCode = code;
    let outputAreaRef = document.getElementById("rx-code");
    outputAreaRef.innerHTML = tapCode;
};

clear = function()
/**
 * Your header documentation here for clear
 *clear function is to reset every global variable, rx-code and rx- translated version to their intital value before the message is sent after the clear button is pressed. 
 */
{
    red = [];green = [];blue = [];
    timeStamp =[];
    colour = [];
    timeRef = timeStamp[0];
    i = 0;
    order = [];
    duration =[]
    timeTaken =[];
    tapCode="";
    N = duration[1];
    
    let outputAreaRef = document.getElementById("rx-code");
    outputAreaRef.innerHTML = null;
    
    let outputAreaRef2 = document.getElementById("rx-translated");
    outputAreaRef2.innerHTML = null;
};
    
/**
 * Your header documentation here for translate
 * Translate function is to convert the tapcode to characters using the relationship stated above.   
 */
translate = function()
{
	//Count the number of taps before every full gap
    let splitCode = tapCode.split("");
    splitCode.push(" ");
    let l=0;
    let k=0;
    let count = [];
    for (let i=0;i<splitCode.length-1;i++)
        {
            if (splitCode[i] === "*")
                {
                    l++;
                }
            else if (splitCode[i] === " ")
                {
                    count[k] = l;
                    l=0;
                    k++;
                }
        }
    
    //Relating the count of taps to respective characters to produce the message 
    let characters = "";
    let row, column;
    
    for (let i=0;i<(count.length-1);i+=2)
        {
            row = count[i];
            column = count[i+1];
            characters += letters[row][column];
        }
    
    //Replacing 3 letter group of wuw with space
    let currChar = "";
    let newChar = "";
    for (let i=0;i<(characters.length)-3 ;i++)
    {
        currChar = characters[i] +characters[i+1] +characters[i+2];
        if (currChar === "wuw")
            {
                newChar = characters.replace(/wuw/g,' ');
                characters = newChar ;
            }
    }
    
    //Replacing 2 letter group of qc with k 
    let currchar ="";
    let newchar = "";
    for (let i=0; i<(characters.length)-1;i++)
        {
           currchar = characters[i]+characters[i+1] ; 
            if (currchar === "qc")
                {
                    newchar = characters.replace(/qc/g,'k');
                    characters = newchar; 
                }
        }
// ERROR CASES    
// Early termination happened when the message ends in a tap or a half gap , therefore an error message will be sent out.
// When there is too many half gaps, the 
let last = order.length-1; 
let errorMessage = "ERROR MESSAGE : EARLY TERMINATION !";
    if ( order[last] === "w")
    {
    characters = errorMessage;
    }
    
    else if ( order[last] === "b") 
    {
        if ( (duration[last] <= 1.5*N)  ){
         characters = errorMessage;
        }
        
        //When there are too many half gaps 
        else if(count.length%2 !== 0){
          characters = "ERROR MESSAGE : TOO MANY HALF GAPS !" ;
        }
    }
     
//WHEN IS TOO MANY SUCCESSIVE TAPS 
    if(tapCode.includes("******")){
        characters = "ERROR MESSAGE : TOO MANY SUCCESSIVE TAPS !";
    }

let outputAreaRef2 = document.getElementById("rx-translated");
outputAreaRef2.innerHTML = characters;
};




